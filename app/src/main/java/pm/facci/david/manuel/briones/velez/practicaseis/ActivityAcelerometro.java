package pm.facci.david.manuel.briones.velez.practicaseis;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;


public class ActivityAcelerometro extends AppCompatActivity implements SensorEventListener{

    TextView textView;
    SensorManager sensorManager;
    private Sensor sensor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acelerometro);
        setTitle(getString(R.string.acelerometro));

        textView = (TextView)findViewById(R.id.LBLAcelerometro);
        sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);


    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, sensor, sensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        float x, y ,z;

        x = sensorEvent.values[0];
        y = sensorEvent.values[1];
        z = sensorEvent.values[2];

        textView.setText(getString(R.string.x) + " " + x  +"\n" + getString(R.string.y) + " " + y + "\n" + getString(R.string.z) + " " + z + "\n\n");

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
