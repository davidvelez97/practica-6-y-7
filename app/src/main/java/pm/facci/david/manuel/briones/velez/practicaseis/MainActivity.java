package pm.facci.david.manuel.briones.velez.practicaseis;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button acelerometro, proximidad, vibra, tempe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        acelerometro = (Button)findViewById(R.id.BTNAcelerometro);
        proximidad = (Button)findViewById(R.id.BTNProximidad);
        vibra = (Button)findViewById(R.id.BTNVibrar);
        tempe = (Button)findViewById(R.id.BTNTemp);

        acelerometro.setOnClickListener(this);
        proximidad.setOnClickListener(this);
        vibra.setOnClickListener(this);
        tempe.setOnClickListener(this);



    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.BTNAcelerometro:
                startActivity(new Intent(this, ActivityAcelerometro.class));
                break;

            case R.id.BTNProximidad:
                startActivity(new Intent(this, ActivityProximidad.class));
                break;

            case R.id.BTNVibrar:
                startActivity(new Intent(this, ActivityVibracion.class));
                break;

            case R.id.BTNTemp:
                startActivity(new Intent(this, ActivityPresion.class));
                break;
        }
    }
}
